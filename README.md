# doctor2patient

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## SDK
```
1,在根目录下创建文件夹sdk,将下载的版本放在sdk文件夹下
2,在根目录下执行命令: npm install ./sdk/rtc_sdk_web_v1.2.1.tar.gz, 安装依赖
3,在项目中引入: import HRTC from 'hrtc'
```
